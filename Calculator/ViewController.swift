//
//  ViewController.swift
//  Calculator
//
//  Created by David on 2/14/16.
//  Copyright © 2016 David. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var screen: UILabel!
    var firstNumber = Double()
    var secondNumber = Double()
    var isTypingNumber = false
    var result = Double()
    var operation = ""
    var screenText = "0"
    var mem = Double()
    
    @IBOutlet weak var lbMem: UILabel!
    @IBOutlet weak var lbErr: UILabel!
    @IBOutlet weak var lbOperation: UILabel!
    
    @IBAction func maOperator(sender: AnyObject) {
        if screenText != "0" && lbErr.text! != "E" {
            if operation != "" {
                equalValue()
            }

            mem = mem + ((screenText as NSString).doubleValue)
            lbMem.text = "M"
        }
    }
    @IBAction func mdOperator(sender: AnyObject) {
        if screenText != "0" && lbErr.text! != "E" {
            if operation != "" {
                equalValue()
            }
            mem = mem - ((screenText as NSString).doubleValue)
            lbMem.text = "M"
        }
    }
    @IBAction func mrOperator(sender: AnyObject) {
        if lbErr.text! != "E" {
            operation = ""
            firstNumber = 0
            secondNumber = 0
            isTypingNumber = false
            lbMem.text = ""
            let numberFormatter = NumberFormatter()
            numberFormatter.positiveFormat = "###0.##"
            

            screenText = numberFormatter.string(from:NSNumber.init(value:mem))!
            displayScreen(data: mem)
        }
    }
    @IBAction func number(sender: AnyObject) {
        if lbErr.text! != "E" {
            let number = sender.currentTitle!
            var couldInput = true
            if isTypingNumber == true && screen.text!.characters.count > 11{
                couldInput = false
            }
            if screen.text! == "0" && (number == "00" || number == "0") && couldInput {
                couldInput = false
            }
            if screen.text!.range(of: ".") != nil && number == "." && couldInput {
                couldInput = false
            }
            if couldInput {
                if isTypingNumber == true {
                    screenText = screenText + number!
                } else {
                    screenText = number!
                }
                displayScreen(data: (screenText as NSString).doubleValue)
                isTypingNumber = true
            }
        }
    }
    
    @IBAction func negative(sender: AnyObject) {
        if lbErr.text! != "E" {
            let numberFormatter = NumberFormatter()
            numberFormatter.positiveFormat = "###0.##"
            screenText = numberFormatter.string(from:NSNumber.init(value:(screenText as NSString).doubleValue * -1))!
            displayScreen(data: (screenText as NSString).doubleValue)
        }
    }
    @IBAction func operation(sender: AnyObject) {
        if lbErr.text! != "E" {
            isTypingNumber = false
            if operation != "" {
                equalValue()
            }
            firstNumber = (screenText as NSString).doubleValue
            operation = sender.currentTitle!!
            lbOperation.text = operation
        }
    }
    
    @IBAction func back(sender: AnyObject) {
        if lbErr.text! != "E" {
            if ( screenText.characters.count > 1 ) {
                screenText.remove(at: screenText.index(before: screenText.endIndex))
                displayScreen(data: (screenText as NSString).doubleValue)
            } else {
                isTypingNumber = false
                displayScreen(data: 0);
            }
        }
    }
    
    @IBAction func equals(sender: AnyObject) {
        if lbErr.text! != "E" && operation != "" {
            equalValue()
        }
    }
    
    func equalValue() {
        secondNumber = (screenText as NSString).doubleValue
        switch operation {
        case "+" :
            result = firstNumber+secondNumber
        case "-" :
            result = firstNumber-secondNumber
        case "×" :
            result = firstNumber*secondNumber
        case "÷" :
            result = firstNumber/secondNumber
        default :
            print("")
        }
        operation = ""
        lbOperation.text = ""
        isTypingNumber = false
        if (result > 999999999999) {
            lbErr.text = "E"
        } else {
            screenText = result.description;
            displayScreen(data: result)
        }
    }
    @IBAction func clearScreen(sender: AnyObject) {
        operation = ""
        displayScreen(data: 0)
        screenText = "0"
        firstNumber = 0
        secondNumber = 0
        isTypingNumber = false
        lbOperation.text = ""
        lbErr.text = ""
        lbMem.text = ""
        mem = 0
        screen.font = screen.font.withSize(70)
    }
    func displayScreen(data: Double) {
        //format data
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        screen.text = "\( numberFormatter.string(from:NSNumber.init(value:data))!)"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        screen.adjustsFontSizeToFitWidth = true
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

